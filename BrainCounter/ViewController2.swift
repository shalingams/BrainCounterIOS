//
//  ViewController2.swift
//  BrainCounter
//
//  Created by Shalinga Manasinghe on 12/24/16.
//  Copyright © 2016 Appzone360. All rights reserved.
//

import UIKit

class ViewController2: UIViewController {
    
    @IBOutlet weak var btnOprator: UIButton!
    @IBOutlet weak var btnNum1: UIButton!
    @IBOutlet weak var btnNum2: UIButton!
    
    @IBOutlet weak var btn0: UIButton!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn5: UIButton!
    @IBOutlet weak var btn6: UIButton!
    @IBOutlet weak var btn7: UIButton!
    @IBOutlet weak var btn8: UIButton!
    @IBOutlet weak var btn9: UIButton!
    @IBOutlet weak var btn10: UIButton!
    @IBOutlet weak var btn11: UIButton!
    @IBOutlet weak var btn12: UIButton!
    @IBOutlet weak var btn13: UIButton!
    @IBOutlet weak var btn14: UIButton!
    @IBOutlet weak var btn15: UIButton!
    
    
    var number1 = 0
    var number2 = 0
    let operators = ["+", "-"]
    var numbers: [Int] = []
    var totalQuestion = 1
    var correctAnswers = 0
    var answer = 0
    var count = 60
    
    @IBOutlet weak var countDownLabel: UILabel!
     
    @IBAction func clickAnswer(_ sender: AnyObject) {
        let selectedAns = sender.titleLabel??.text
        
        if String(answer) == selectedAns {
            correctAnswers += 1
            count = count + 3
        }else{
            count = count - 2
        }
        getQuestions()

    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        You have 60 seconds to answer meny question you can, every correct answer will add 3 seconds and wrong answer will deduct 2 seconds from timer. GOOD LUCK
        getQuestions()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "start_menu.png")!)
         
        _ = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ViewController2.update), userInfo: nil, repeats: true)
        for i in 1...99 {
            numbers.append(i)
        }
    }
    
    func update() {
        count = count-1
        if(count < 1) {
            print(String(correctAnswers))
            performSegue(withIdentifier: "showScore", sender: self)
        }else{
            countDownLabel.text = String(count)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let DestView = segue.destination as! ViewController3
        
        if segue.identifier == "showScore" {
            DestView.totalQuestion  = totalQuestion
            DestView.correctAnswers = correctAnswers
            DestView.incorrectAnswers = totalQuestion - correctAnswers
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getQuestions(){
        totalQuestion += 1
        let randomIndex = Int(arc4random_uniform(UInt32(operators.count)))
        var op = operators[randomIndex]
        var result: [Int] = []
        
        let num1 = Int(arc4random_uniform(99) + 1)
        let num2 = Int(arc4random_uniform(99) + 1)
        
        if num1 > num2 {
            op = "-"
        }
        
        if num1 < num2 && op == "-" {
            op = "+"
        }

        if op == "-" {
            answer = num1 - num2
            btnOprator.setImage(UIImage(named: "minus.png"), for: UIControlState.normal)
        }else{
            answer = num1 + num2
            btnOprator.setImage(UIImage(named: "plus.png"), for: UIControlState.normal)
        }
        
        btnOprator.setTitle(op,for: .normal)
        btnNum1.setTitle(String(num1),for: .normal)
        btnNum2.setTitle(String(num2),for: .normal)
        
        for _ in 0...14 {
            result.append(Int(arc4random_uniform(199) + 1))
        }
        
        result.append(answer)
        
        for index in ((0 + 1)...result.count - 1).reversed()
        {
            let j = Int(arc4random_uniform(UInt32(index-1)))
            swap(&result[index], &result[j])
        }
        
        btn0.setTitle(String(result[0]),for: .normal)
        btn1.setTitle(String(result[1]),for: .normal)
        btn2.setTitle(String(result[2]),for: .normal)
        btn3.setTitle(String(result[3]),for: .normal)
        btn4.setTitle(String(result[4]),for: .normal)
        btn5.setTitle(String(result[5]),for: .normal)
        btn6.setTitle(String(result[6]),for: .normal)
        btn7.setTitle(String(result[7]),for: .normal)
        btn8.setTitle(String(result[8]),for: .normal)
        btn9.setTitle(String(result[9]),for: .normal)
        btn10.setTitle(String(result[10]),for: .normal)
        btn11.setTitle(String(result[11]),for: .normal)
        btn12.setTitle(String(result[12]),for: .normal)
        btn13.setTitle(String(result[13]),for: .normal)
        btn14.setTitle(String(result[14]),for: .normal)
        btn15.setTitle(String(result[15]),for: .normal)
    }

}
