//
//  ViewController3.swift
//  BrainCounter
//
//  Created by Shalinga Manasinghe on 12/27/16.
//  Copyright © 2016 Appzone360. All rights reserved.
//

import UIKit

class ViewController3: UIViewController {

    var totalQuestion = 0
    var correctAnswers = 0
    var incorrectAnswers = 0
    
    @IBOutlet weak var lblTot: UILabel!
    @IBOutlet weak var lblAnswerd: UILabel!
    @IBOutlet weak var lblWrong: UILabel!
    
    
    @IBAction func exitApp(_ sender: AnyObject) {
        exit(0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "start_menu.png")!)
        lblTot.text = String(totalQuestion)
        lblAnswerd.text = String(correctAnswers + incorrectAnswers)
        lblWrong.text = String(incorrectAnswers)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
